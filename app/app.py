import asyncio
import random
import asyncpg
import validators
from os import getenv
from aiogram import Bot, Dispatcher
from aiogram.filters import CommandStart, Command
from aiogram.utils.formatting import Text, BotCommand, Url
from aiogram.types import Message

TOKEN = getenv("TG_TOKEN")
dp = Dispatcher()


async def establish_db_connection():
    global conn
    conn = await asyncpg.connect(
        user=getenv("DB_USER"),
        password=getenv("DB_PASSWORD"),
        database=getenv("DB_NAME"),
        port=getenv("DB_PORT"),
        host="db",
    )


@dp.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    content = Text("Привет! Я бот, который поможет не забыть прочитать статьи, "
        "найденные тобой в интернете :)\n"
        " – Чтобы я запомнил статью, достаточно передать мне ссылку на нее. "
        "К примеру, https://example.com\n"
        " – Чтобы получить случайную статью, достаточно передать мне команду ",
        BotCommand("/get_article"),
        ".\n Но помни! Отдавая статью тебе на прочтение, она больше не хранится"
        "в моей базе. Так что тебе точно нужно ее изучить.")
    await message.answer(**content.as_kwargs())


@dp.message(Command("get_article"))
async def get_article(message: Message):
    user_id = message.chat.id
    rows = await conn.fetch("SELECT (link) FROM links WHERE user_id = $1 AND was_used = FALSE", user_id)
    data = [dict(row) for row in rows]
    if data:
        choice = random.randint(0, len(data) - 1)
        link = data[choice]["link"]
        await conn.execute("UPDATE links SET was_used = TRUE WHERE user_id = $1 AND link = $2 AND was_used = FALSE",
                           user_id, link)
        content = Text("Вы хотели прочитать ", Url(link)," \nСамое время это сделать!")
        await message.reply(**content.as_kwargs())
    else:
        await message.reply(
            "Вы пока не сохранили ни одной статьи:)\n"
            "Если нашли что-то стоящее, я жду!"
        )


@dp.message()
async def save_article(message: Message):
    user_id = message.chat.id
    link = message.text
    if not validators.url(link):
        await message.reply("Не является корректным URL")
        return
    
    rows = await conn.fetch(
        "SELECT * FROM links WHERE user_id = $1 AND link = $2 AND was_used = FALSE", user_id, link
    )
    data = [dict(row) for row in rows]
    if data:
        await message.reply("Упс, вы это уже сохраняли :)")
    else:
        await conn.execute(
            "INSERT INTO links(user_id, link, was_used) VALUES($1, $2, FALSE)",
            user_id,
            link,
        )
        await message.reply("Сохранил, спасибо!")


async def main() -> None:
    bot = Bot(TOKEN)
    conn = await establish_db_connection()

    try:
        await dp.start_polling(bot)
    finally:
        await conn.close()


if __name__ == "__main__":
    asyncio.run(main())
